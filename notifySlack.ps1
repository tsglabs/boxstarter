#Vars
$DesktopPath = [Environment]::GetFolderPath("Desktop")

#Setup checks
if (!(Test-Path "$DesktopPath\apikeys.ps1"))
{
    Write-Host "Cannot find apikeys.ps1 on in $DesktopPath. Closing"
    $x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    exit
}
else
{
    . $DesktopPath\apikeys.ps1
    
    #Install a module needed for Powershell -> Slack notifications
    Install-Module PSSlack
    Import-Module PSSlack
    Send-SlackMessage -Uri $slackURI -Channel 'itnotifications' -Parse full -Text 'A Boxstarter script on $env:computername has completed. Just letting the @channel know, especially @samt'
}



