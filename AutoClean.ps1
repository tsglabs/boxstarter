# Load assembly
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

#Vars
$DesktopPath = [Environment]::GetFolderPath("Desktop")

#Delete git cloned repo
if (Test-Path "$DesktopPath\boxstarter-launchers")
{
    #Remove-Item $DesktopPath\boxstarter-launchers -Force -Recurse
}

[System.Windows.Forms.Messagebox]::Show("Cleaning done")

#Reboot if needed
if (Test-PendingReboot) { Invoke-Reboot }