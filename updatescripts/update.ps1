#Install the latest powershell (if not already installed) and then update all installed Chocolatey packages

#Check if ExecutionPolicy = Bypass
$Policy = "Bypass"
If ((get-ExecutionPolicy) -eq $Policy)
{
    #Check if Chocolatey is installed
    if (Test-Path "$($env:ProgramData)\chocolatey\choco.exe")
    {
        #Always install the latest Powershell
        cinst powershell -yr

        #Now update
        cup all
    }
    else
    {
        #Chocolatey isn't installed
    }
}