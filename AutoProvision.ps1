#Vars
$DesktopPath = [Environment]::GetFolderPath("Desktop")
$NETKeyLocation = "HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full"

Write-Host "= Automatically provision a new PC with Chocolatey and Boxstarter ="
Write-Host "This script will:"
Write-Host "> Check the current ExecutionPolicy"
Write-Host "> Check if .NET 4.0+ is installed, install it if not, and restart the PC"
Write-Host "> Check if Chocolatey is installed, and install it if not"
Write-Host "> Install Boxstarter"
Write-Host "> Install Git"
Write-Host "> Clone the boxstarter-launchers repository"

#Check if ExecutionPolicy = Bypass
Write-Host "== Checking ExecutionPolicy =="
$Policy = "Bypass"
If ((get-ExecutionPolicy) -ne $Policy)
{
    $formattedTime = Get-Date -Format "[%H:%m:%s]: "
    Write-Host $formattedTime "ExecutionPolicy is not Bypass, attempting to set it to Bypass"
    Set-ExecutionPolicy $Policy -Force
    Write-Host "### Please re-run this script ###"
    $x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    exit
}
else
{
    $formattedTime = Get-Date -Format "[%H:%m:%s]: "
    Write-Host $formattedTime "ExecutionPolicy is set to Bypass"

    #Check if .NET 4.0+ is installed
    Write-Host "== Checking if .NET 4.0+ is installed =="
    $KeyExists = Test-Path $NETKeyLocation
    if ($KeyExists -eq $True)
    {
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Found the registry key relating to .NET 4.0+, so we dont't need to install it. Skipping..."
    }
    else
    {
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Can't find the registry key relating to .NET 4.0+, so it's probably not installed. Installing now..."
        $url = "https://tsg-internal.uk/static/exe/dotNetFx40_Full_setup.exe"
        $outpath = "$DesktopPath/dotNetFx40_Full_setup.exe"
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Downloading dotNetFx40_Full_setup.exe from $url"
        $wc = New-Object System.Net.WebClient
        $wc.DownloadFile($url, $outpath)
        Start-Process -FilePath $outpath -Wait
        exit
    }

    #Install chocolatey if its not already installed
    Write-Host "== Checking if chocolatey is installed =="
    if (!(Test-Path "$($env:ProgramData)\chocolatey\choco.exe"))
    {
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Chocolatey doesn't appear to be installed, attempting to install now..."
        Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    }
    else
    {
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "It's already installed!"
    }


    #Now, install Boxstarter
    Write-Host "== Installing Boxstarter =="
    if (Test-Path "$($env:ProgramData)\chocolatey\choco.exe")
    {
        #Update chocolatey
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Updating chocolatey first..."
        cup chocolatey -yr

        #Install the latest Powershell
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Getting the latest powershell..."
        cinst powershell -yr

        #Install Boxstarter
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Installing Boxstarter..."
        cinst boxstarter -yr

        #Install Git
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Installing Git..."
        cinst git.install -yr
    }
    else
    {
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Can't find chocolatey, something went wrong here..."
        $formattedTime = Get-Date -Format "[%H:%m:%s]: "
        Write-Host $formattedTime "Press any key to exit"
        $x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    }
    
    #Refresh Environment Vars
    refreshenv
    
    #Download repository so 
    & 'C:\Program Files\Git\bin\git.exe' clone https://gitlab.com/tsglabs/boxstarter-launchers.git $DesktopPath/boxstarter-launchers

    #All done!
    $formattedTime = Get-Date -Format "[%H:%m:%s]: "
    Write-Host $formattedTime "Done!"
    Write-Host $formattedTime "Press any key to exit and reboot"
    $x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    shutdown /r /t 0
}
